FROM ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    pandoc \
    texlive \
    build-essential \
    context \
    curl

RUN sh -c "$(curl -ssL https://taskfile.dev/install.sh)" -- -d
