Álvaro Bermejo
==============

---------------------------------------- -----------------------------------
~![mail]~ [cv@alvarber.com]                    ~![briefcase]~ [alvarber.com]
~![linkedin]~ [linkedin.com/in/alvarber]   ~![github]~ [github.com/AlvarBer]
---------------------------------------- -----------------------------------

Professional Experience
----------------------
1 Year - - - (Remote) - ⁣⁣⁣⁣⁣[Ongoing]
  ~ **Technical Artist** at King
  ~ Creating a pipeline to produce video content for Marketing campaign.
  ~ Making all kind of tools for the team.

9 months - (Remote)
  ~ **Freelancer Technical Artist** at various companies
  ~ Focused on VFX and shader work.
  ~ Also wrote AI & procedural generation code.

3 months - (Madrid)
  ~ **Unity Developer** at Pendulo Studios
  ~ Worked on UI and tooling for the 3D pipeline.
  ~ Improved Blender tools and workflow.

6 months - (London)
  ~ **Unity Game Developer** at Funky Panda Games
  ~ Gameplay programmer, with additional UI, Shader & backend duties.
  ~ Worked on [Stardust] in UI, gameplay and optimization.
  ~ Rapid prototyping of hyper-casual games for Android & ios.

18 Months - (London)
  ~ **Software Engineer** at Smarkets
  ~ Moved the existing infrastructure to Kubernetes.

Personal Projects
-----------------
[From the End of the World]
  ~ A narrative game about mending your broken heart in a remote island.
  ~ 3D lo-fi aesthetic, uses Yarn for the dialogue system.

[Shine on]
  ~ A local co op game where lights are used to fend the ghosts away.
  ~ Created a fake 2d volumetric light with polygon colliders.

[Linked Space]
  ~ A 2D split-screen game about interlinked physics between two worlds.
  ~ Parallel worlds where objects on one side move on the other.

Skills
------
~![code]~ Dev

  ~ Shaders, both in **CG/HLSL** and **Shader Graph**.
  
  ~ Game programming with **Unity** in **C#** and **Python**.

  ~ 3D Modelling with **Blender**, as well as prototyping with **Probuilder**.

  ~ Procedural Generation using **Houdini**.

~![globe]~ Languages

  ~ English and Spanish.

Education
---------
2020 - 2021
:   **Masters in Technical Art and VFX** at Voxel School (Madrid, Spain).

    Aimed at Houdini and Unreal Engine for VFX and procedural generation.

2016 - 2017
:   **BSc, Computer Science (Artificial Intelligence)** at Hertfordshire
    University (Hatfield, UK).

    Year abroad as part of the Erasmus programme, awarded best undergrad project.

2013 - 2017
:   **Computer Science Engineering** at Universidad Complutense (Madrid, Spain).

    A 4-year engineering programme focused on computer science theory.

<!-- Images -->
[mail]: images/envelope.png
[github]: images/github.png
[linkedin]: images/linkedin-square.png
[code]: images/code.png
[code-fork]: images/code-fork.png
[globe]: images/globe.png
[briefcase]: images/briefcase.png

<!-- Double links -->
[cv@alvarber.com]: mailto:cv@alvarber.com
[github.com/AlvarBer]: https://github.com/AlvarBer
[alvarber.com]: https://alvarber.com
[linkedin.com/in/alvarber]: https://www.linkedin.com/in/alvarber

<!-- Projects links -->
[Stardust]: https://play.google.com/store/apps/details?id=com.playstack.stardust
[Shine on]: https://noisybass.itch.io/shine-on
[Linked Space]: https://alvarber.itch.io/linked-space
[From the End of the World]: https://alvarber.itch.io/from-the-end-of-the-world
