Good morning!

I'm Álvaro, I'm a game programmer with experience developing games in Unity.

[Paragraph telling why I applied/Something related to requirements]

The bulk of my experience is split among mobile games I made professionally (I worked on startdust) and web games I created either personally or as part of a team in a game jam (For those my itch it's the place to check them, I think Linked Space is one of the best examples). For a more overview of my work I have a personal webpage.

I've worked mostly on gameplay, UI, optimization, and shaders. I've also done a bit of procedural generation and AI stuff here and there.

[Paragraph saying why I would love to work with them]
I would love to work with you folks, I believe I have the capabilities, and I can learn whatever skills I lack in a short time.

Awaiting your response,
Álvaro.