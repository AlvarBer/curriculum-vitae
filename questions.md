• What does a normal day/week look like on this team?
• What would you say makes someone successful in this position?
• What makes a good culture fit for your studio?
• What type of traits do you think would fit well within the culture you're trying to establish in the workplace?
• How is work divided up on the team?
• What do you do to enable and encourage skill and career growth in your employees?
• How do you promote diversity and inclusion? How do you create a safe environment for everyone on the team?
• Are there opportunities for advancement/how do promotions typically work?
• How do you like working at ___?
• What has been your favorite project to work on so far?
• How does remote work look like for you right now? How do you think it's going?
• What is your plan for returning back to work? When?
• How will returning back to work affect new hires?
• Is there relocation assistance?
• What would be the potential next steps in the interview process?
• When can I expect to hear back?
• Can I get contact information for you in case I have any additional questions?