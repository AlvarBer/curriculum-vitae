Álvaro Bermejo
==============

---------------------------------------- -----------------------------------
~![mail]~ [cv@alvarber.com]                    ~![briefcase]~ [alvarber.com]
~![linkedin]~ [linkedin.com/in/alvarber]   ~![github]~ [github.com/AlvarBer]
---------------------------------------- -----------------------------------

Professional Experience
----------------------
2 months - (Madrid)
  ~ **Game Programmer** at Pendulo Studios
  ~ Worked on UI and tooling for the 3D pipeline.
  ~ Improved Blender tools and workflow.

6 months - (London)
  ~ **Unity Game Programmer** at Funky Panda Games
  ~ Gameplay programmer, with additional UI, Shader & backend duties.
  ~ Rapid prototyping of games for Android & ios.
  ~ Worked on games like [Stardust] & other unreleased titles

6 months - (Remote)
  ~ **Freelancer** at various companies
  ~ Focused on Unity and shader programming.
  ~ Also worked on AI & procedural generation tasks.

14 months - (London)
  ~ **Software Engineer** at Smarkets
  ~ Mainly focused on moving the infrastructure to Kubernetes.

Selected Projects
-----------------
[From the End of the World]
  ~ A narrative game about mending your broken heart in a remote island.
  ~ 3D lo-fi aesthetic, uses Yarn for the dialogue system.

[Shine on]
  ~ A co op game where lights are used to fend the ghosts away.
  ~ Worked on procedural meshes with 2D polygon colliders.

[Linked Space]
  ~ A 2D split-screen game about interlinked physics between two worlds.
  ~ Parallel worlds where objects on one side move on the other.

[KatTotem]
  ~ A local co op game where one player builds a totem while the other
    knocks it down.
  ~ Wrote shaders for scene transitions, the line boil, shockwave effect and
    more.

Skills
------
~![code]~ Dev

  ~ Game programming with **Unity** in **C#**.

  ~ Shaders, both in **CG/HLSL** and **Shader Graph**.

  ~ Machine Learning in Python **scikit-learn** and **Matlab**.

~![globe]~ Languages

  ~ English and Spanish.

Education
---------
2020 - 2021
:   **Masters in Technical Art and VFX** at Voxel School (Madrid, Spain).

    Aimed at Houdini and Unreal Engine for VFX and procedural generation.

2016 - 2017
:   **BSc, Computer Science (Artificial Intelligence)** at Hertfordshire
    University (Hatfield, UK).

    Year abroad as part of the Erasmus programme, awarded best undergrad project.

2013 - 2017
:   **Computer Science Engineering** at Universidad Complutense (Madrid, Spain).

    A 4-year engineering programme focused on computer science theory.

<!-- Images -->
[mail]: images/envelope.png
[github]: images/github.png
[linkedin]: images/linkedin-square.png
[code]: images/code.png
[code-fork]: images/code-fork.png
[globe]: images/globe.png
[briefcase]: images/briefcase.png

<!-- Double links -->
[cv@alvarber.com]: mailto:cv@alvarber.com
[github.com/AlvarBer]: https://github.com/AlvarBer
[alvarber.com]: https://alvarber.com
[linkedin.com/in/alvarber]: https://www.linkedin.com/in/alvarber

<!-- Projects links -->
[Stardust]: https://play.google.com/store/apps/details?id=com.playstack.stardust
[Shine on]: https://noisybass.itch.io/shine-on
[Linked Space]: https://alvarber.itch.io/linked-space
[KatTotem]: https://alvarber.itch.io/kattotem
[From the End of the World]: https://alvarber.itch.io/from-the-end-of-the-world