Buenos días!

Me llamo Ávaro, soy un programador de videojuegos con experiencia en Unity.

La mayor parte de mi experiencia profesional ha sido desarrollando para móviles (Uno de los proyectos donde he trabajado es stardust [https://play.google.com/store/apps/details?id=com.playstack.stardust]), pero también he trabajado en proyectos de web y pc, más en el contexto de proyectos personales y game jams (Tengo casi todos en [https://alvarber.itch.io/], Linked Space es probablemente uno de los mejores ejemplos) También tengo algunas cosas más interesantes en mi página web [https://alvarber.gitlab.io/].

He trabajado principalmente en gameplay, UI, optimización y shaders. También he trabajado puntualmente en generación procedimental y AI.

[Paragraph explaining why I want to work with them].

Un saludo,
Álvaro.